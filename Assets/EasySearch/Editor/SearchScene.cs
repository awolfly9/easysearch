﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace SearchTool
{
    public class SearchScene : EditorWindow
    {
		StateName _state;

		int _SearchLayer;
		string _SearchTag = "Untagged";
		string _scriptName = "UnKown";
        string _textureName = "UnKown";

		GameObjectInfo[] _result;

		Vector2 _scrollPos;

        [MenuItem("EasySearch/SearchScene")]
		public static void ShowWindow()
		{
			SearchScene win = EditorWindow.GetWindow<SearchScene>(true);
			win.title = "SearchScene";
			win.minSize = new Vector2(800, 480);
			win.Show();
		}

		void OnGUI()
		{
			switch (_state)
			{
				case StateName.Input:
					{
						SearchByLayer();
						SearchByTag();
						SearchByScriptName();
					}
					break;
				case StateName.ShowResult:
					if (GUILayout.Button("Clear Result"))
					{
						_state = StateName.Input;
					}
					EditorGUILayout.Space();
					if (_result != null)
					{
						_scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
						foreach (var item in _result)
						{
							EditorGUILayout.BeginHorizontal();
							EditorGUILayout.LabelField(item.Path);
							EditorGUILayout.ObjectField(item.gameObject, typeof(GameObject), true, GUILayout.Width(180));
							EditorGUILayout.EndHorizontal();
						}
						EditorGUILayout.EndScrollView();
					}
					break;
				default:
					break;
			}
		}

		void SearchByLayer()
		{
			_SearchLayer = EditorGUILayout.LayerField("Layer", _SearchLayer);
			if (GUILayout.Button("Search by layer"))
			{
				List<GameObjectInfo> list = new List<GameObjectInfo>();
                UnityEngine.Object[] objs = EditorWindow.FindObjectsOfType(typeof(GameObject));
				foreach (GameObject go in objs)
				{
					if (go.layer == _SearchLayer)
						list.Add(new GameObjectInfo(go));
				}
				_result = list.ToArray();
				_state = StateName.ShowResult;
			}
			EditorGUILayout.Space();
		}

		void SearchByTag()
		{
			_SearchTag = EditorGUILayout.TagField("Tag", _SearchTag);
			if (GUILayout.Button("Search by tag"))
			{
				List<GameObjectInfo> list = new List<GameObjectInfo>();
                UnityEngine.Object[] objs = EditorWindow.FindObjectsOfType(typeof(GameObject));
				foreach (GameObject go in objs)
				{
					if (go.tag == _SearchTag)
						list.Add(new GameObjectInfo(go));
				}
				_result = list.ToArray();
				_state = StateName.ShowResult;
			}

			EditorGUILayout.Space();
		}
		
        void SearchByTexture()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Texture");
            _textureName = EditorGUILayout.TextField(_textureName);
            EditorGUILayout.EndHorizontal();
            if (GUILayout.Button("Search by texture"))
            {
                List<GameObjectInfo> list = new List<GameObjectInfo>();
                UnityEngine.Object[] objs = EditorWindow.FindObjectsOfType(typeof(Texture));
                foreach (GameObject go in objs)
                {
                    if (go.GetComponent(_scriptName) != null)
                    {
                        list.Add(new GameObjectInfo(go));
                    }
                }
                _result = list.ToArray();
                _state = StateName.ShowResult;
            }
            EditorGUILayout.Space();
        }


		void SearchByScriptName()
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("ScriptName");
			_scriptName = EditorGUILayout.TextField(_scriptName);
			EditorGUILayout.EndHorizontal();
			if (GUILayout.Button("Search by script name"))
			{
				List<GameObjectInfo> list = new List<GameObjectInfo>();
                UnityEngine.Object[] objs = EditorWindow.FindObjectsOfType(typeof(GameObject));
				foreach (GameObject go in objs)
				{
					if (go.GetComponent(_scriptName) != null)
					{
						list.Add(new GameObjectInfo(go));
					}
				}
				_result = list.ToArray();
				_state = StateName.ShowResult;
			}
			EditorGUILayout.Space();
		}

		enum StateName
		{
			Input,
			ShowResult,
		}

		class GameObjectInfo
		{
			public GameObject gameObject;
			public string Path;

			public GameObjectInfo(GameObject go)
			{
				gameObject = go;

				Path = gameObject.name;
				Transform t = gameObject.transform.parent;
				while (t != null)
				{
					Path = string.Format("{0}/{1}", t.name, Path);
					t = t.parent;
				}
			}
		}
    }
}
