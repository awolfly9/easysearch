﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace SearchTool
{
	public class SearchPrefab : EditorWindow
	{
		StateName _state = StateName.Input;

		int _SearchLayer;
		string _SearchTag = "Untagged";
		string _objName = "UnKown";
		string _scriptName = "UnKown";

		GameObjectInfo[] _result;
		List<GameObjectInfo> _tempList = new List<GameObjectInfo>();
		Vector2 _scrollPos;

		[MenuItem("EasySearch/SearchPrefab")]
		static void ShowWindow()
		{
			SearchPrefab win = EditorWindow.GetWindow<SearchPrefab>(true);
			win.title = "SearchPrefab";
			win.minSize = new Vector2(800, 480);
			win.Show();
		}

		void OnGUI()
		{
			switch (_state)
			{
				case StateName.Input:
					{
						SearchByLayer();
						SearchByTag();
						SearchByObjName();
						SearchByScriptName();
					}
					break;
				case StateName.ShowResult:
					if (GUILayout.Button("Clear Result"))
					{
						_tempList.Clear();
						_state = StateName.Input;
					}
					EditorGUILayout.Space();
					if (_result != null)
					{
						_scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
						foreach (var item in _result)
						{
							EditorGUILayout.BeginHorizontal();
							EditorGUILayout.LabelField(item.path);
							EditorGUILayout.ObjectField(item.parent, typeof(GameObject), true, GUILayout.Width(180));
							EditorGUILayout.ObjectField(item.gameObject, typeof(GameObject), true, GUILayout.Width(180));
							EditorGUILayout.EndHorizontal();
						}
						EditorGUILayout.EndScrollView();
					}
					break;
				default:
					break;
			}
		}

		void FindPrefab(GameObject obj, SearchType type, bool isRecursion = false)
		{
			for (int i = 0; i < obj.transform.childCount; ++i)
			{
				Transform transform = obj.transform.GetChild(i);
				if (transform == null)
				{
					continue;
				}

				switch (type)
				{
					case SearchType.Layer:
						{
							if (transform.gameObject.layer == _SearchLayer)
							{
								_tempList.Add(new GameObjectInfo(transform.gameObject));
							}
						}
						break;
					case SearchType.Tag:
						{
							if (transform.gameObject.tag == _SearchTag)
							{
								_tempList.Add(new GameObjectInfo(transform.gameObject));
							}
						}
						break;
					case SearchType.ObjName:
						{
							if (transform.gameObject.name.ToLower() == _objName.ToLower())
							{
								_tempList.Add(new GameObjectInfo(transform.gameObject));
							}
						}
						break;
					case SearchType.ScriptName:
						{
							if (transform.GetComponent(_scriptName) != null)
							{
								_tempList.Add(new GameObjectInfo(transform.gameObject));
							}
						}
						break;
					default:
						break;
				}

				if (isRecursion)
				{
					FindPrefab(transform.gameObject, type);
				}
			}
		}

		void SearchByLayer()
		{
			_SearchLayer = EditorGUILayout.LayerField("Layer", _SearchLayer);
			if (GUILayout.Button("Search by layer"))
			{
				string[] prefabs = AssetDatabase.FindAssets("t:Prefab");

				for (int i = 0; i < prefabs.Length; ++i)
				{
					string path = AssetDatabase.GUIDToAssetPath(prefabs[i]);
					GameObject obj = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
					FindPrefab(obj, SearchType.Layer);
					if (obj.layer == _SearchLayer)
					{
						_tempList.Add(new GameObjectInfo(obj));
					}
				}

				_result = _tempList.ToArray();
				_state = StateName.ShowResult;
			}
			EditorGUILayout.Space();
		}

		void SearchByTag()
		{
			_SearchTag = EditorGUILayout.TagField("Tag", _SearchTag);
			if (GUILayout.Button("Search by tag"))
			{
				string[] prefabs = AssetDatabase.FindAssets("t:Prefab");

				for (int i = 0; i < prefabs.Length; ++i)
				{
					string path = AssetDatabase.GUIDToAssetPath(prefabs[i]);
					GameObject obj = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
					FindPrefab(obj, SearchType.Tag);
					if (obj.tag == _SearchTag)
					{
						_tempList.Add(new GameObjectInfo(obj));
					}
				}

				_result = _tempList.ToArray();
				_state = StateName.ShowResult;
			}

			EditorGUILayout.Space();
		}

		void SearchByObjName()
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("GameObjectName");
			_objName = EditorGUILayout.TextField(_objName);
			EditorGUILayout.EndHorizontal();
			if (GUILayout.Button("Search by gameobject name"))
			{
				string[] prefabs = AssetDatabase.FindAssets("t:Prefab");
				for (int i = 0; i < prefabs.Length; ++i)
				{
					string path = AssetDatabase.GUIDToAssetPath(prefabs[i]);
					GameObject obj = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
					FindPrefab(obj, SearchType.ObjName, true);
					if (obj.name == _objName)
					{
						_tempList.Add(new GameObjectInfo(obj));
					}
				}

				_result = _tempList.ToArray();
				_state = StateName.ShowResult;
			}
			EditorGUILayout.Space();
		}

		void SearchByScriptName()
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("ScriptName");
			_scriptName = EditorGUILayout.TextField(_scriptName);
			EditorGUILayout.EndHorizontal();
			if (GUILayout.Button("Search by script name"))
			{
				string[] prefabs = AssetDatabase.FindAssets("t:Prefab");
				for (int i = 0; i < prefabs.Length; ++i)
				{
					string path = AssetDatabase.GUIDToAssetPath(prefabs[i]);
					GameObject obj = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
					FindPrefab(obj, SearchType.ScriptName, true);
					if (obj.transform.GetComponent(_scriptName) != null)
					{
						_tempList.Add(new GameObjectInfo(obj));
					}
				}

				_result = _tempList.ToArray();
				_state = StateName.ShowResult;
			}
			EditorGUILayout.Space();
		}

		enum StateName
		{
			Input,
			ShowResult,
		}

		enum SearchType
		{
			Layer,
			Tag,
			ObjName,
			ScriptName,
		}

		class GameObjectInfo
		{
			public GameObject parent;
			public GameObject gameObject;
			public string path;

			public GameObjectInfo(GameObject go)
			{
				gameObject = go;
				path = gameObject.name;
				Transform t = gameObject.transform.parent;
				while (t != null)
				{
					path = string.Format("{0}/{1}", t.name, path);
					parent = t.gameObject;
					t = t.parent;					
				}
			}
		}
	}
}
